(async () => {

const options = await messenger.storage.local.get();

if (options.mailto_link_disable_click)
{
    document.body.addEventListener('click', disableClick);
}

registerCssStyles();
applyStyles();

function registerCssStyles()
{
    if (Object.keys(options).length == 0) { return }

    let styleElement = document.createElement('style');
    document.head.appendChild(styleElement);

    let mailtoLinkRemoveUnderline = options.mailto_link_remove_underline ? 'text-decoration: none !important;' : '';
    let mailtoLinkStyle = `
        .__anchor_mailto {
            ${mailtoLinkRemoveUnderline}
            color: ${options.mailto_link_color} !important
        }`;
    let mailtoLinkHoverStyle = `
        .__anchor_mailto:hover {
            text-decoration: underline !important;
            color: ${options.mailto_link_hover_color} !important
        }`;
        
    styleElement.sheet.insertRule(mailtoLinkStyle, styleElement.sheet.cssRules.length);
    styleElement.sheet.insertRule(mailtoLinkHoverStyle, styleElement.sheet.cssRules.length);

    let otherLinkRemoveUnderline = options.other_link_remove_underline ? 'text-decoration: none !important;' : '';
    let otherLinkStyle = `
        .__anchor_other {
            ${otherLinkRemoveUnderline}
            color: ${options.other_link_color} !important
        }`;
    let otherLinkHoverStyle = `
        .__anchor_other:hover {
            text-decoration: underline !important;
            color: ${options.other_link_hover_color} !important
        }`;
        
    styleElement.sheet.insertRule(otherLinkStyle, styleElement.sheet.cssRules.length);
    styleElement.sheet.insertRule(otherLinkHoverStyle, styleElement.sheet.cssRules.length);
}

function applyStyles()
{
    let anchors = document.querySelectorAll('a');
    
    for (let a of anchors)
    {
        if (a.href.startsWith('mailto:'))
        {
            if (options.mailto_link_change_appearance)
            {
                // All anchors with mailto URI will be changed without exception.
                if (options.mailto_link_no_exception)
                {
                    a.classList.add('__anchor_mailto');
                }
                else
                {
                    let hrefContent = a.href.split(':');
                    let textContent = a.textContent.trim();
                    
                    if ((hrefContent.length == 2 && (textContent == hrefContent[1] || textContent == a.href || textContent == `<${hrefContent[1]}>`)) ||
                        (hrefContent.length == 3 && textContent == `${hrefContent[1]}:${hrefContent[2]}`))
                    {
                        a.classList.add('__anchor_mailto');
                    }
                }
            }
        }
        else
        {
            if (options.other_link_change_appearance)
            {
                a.classList.add('__anchor_other');
            }
        }
    }
}

function disableClick(e)
{
    if (e.target.nodeName == 'A' && e.target.href.startsWith('mailto:'))
    {
        e.stopPropagation();
        e.preventDefault();
    }
}

})();

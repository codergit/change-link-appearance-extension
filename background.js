async function createDefaultSetting()
{
    let setting = await messenger.storage.local.get();

    if (Object.keys(setting).length)
    {
        // Setting already exists. Do nothing.
    }
    else
    {
        messenger.storage.local.set(
            {
                mailto_link_change_appearance: true,
                mailto_link_no_exception: false,
                mailto_link_remove_underline: true,
                mailto_link_color: '#000000',
                mailto_link_hover_color: '#1e90ff',
                mailto_link_disable_click: true,

                other_link_change_appearance: true,
                other_link_remove_underline: true,
                other_link_color: '#1e90ff',
                other_link_hover_color: '#1e90ff'
            }
        );
    }
}

function main()
{
    createDefaultSetting();
    messenger.messageDisplay.onMessageDisplayed.addListener((tab, _message) => messenger.tabs.executeScript(tab.id, {file: 'contentscript.js'}));
}

main();

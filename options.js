(async () => {

const options = await messenger.storage.local.get();

let mailtoLinkChangeAppearance = document.getElementById('mailto-link-change-appearance'),
    mailtoLinkNormalColor = document.getElementById('mailto-link-color'),
    mailtoLinkHoverColor = document.getElementById('mailto-link-hover-color'),
    mailtoLinkRemoveUnderline = document.getElementById('mailto-link-remove-underline'),
    mailtoLinkDisableClick = document.getElementById('mailto-link-disable-click'),
    mailtoLinkNoException = document.getElementById('mailto-link-no-exception');

mailtoLinkChangeAppearance.checked = options.mailto_link_change_appearance;
mailtoLinkNormalColor.value = options.mailto_link_color;
mailtoLinkHoverColor.value = options.mailto_link_hover_color;
mailtoLinkRemoveUnderline.checked = options.mailto_link_remove_underline;
mailtoLinkDisableClick.checked = options.mailto_link_disable_click;
mailtoLinkNoException.checked = options.mailto_link_no_exception;

if (mailtoLinkChangeAppearance.checked == false)
{
    document.querySelector('.appearance-row').classList.add('disabled');
    mailtoLinkNormalColor.disabled = mailtoLinkHoverColor.disabled = mailtoLinkRemoveUnderline.disabled = true;
}

mailtoLinkChangeAppearance.onchange = (e) =>
{
    let row = e.target.closest('div.box').querySelector('div.appearance-row');
    mailtoLinkNormalColor.disabled = mailtoLinkHoverColor.disabled = mailtoLinkRemoveUnderline.disabled = !e.target.checked;

    if (e.target.checked)
    {
        row.classList.remove('disabled');
    }
    else
    {
        row.classList.add('disabled');
    }

    messenger.storage.local.set({ mailto_link_change_appearance: e.target.checked });
}

mailtoLinkNormalColor.oninput = (e) => messenger.storage.local.set({ mailto_link_color: e.target.value });
mailtoLinkHoverColor.oninput = (e) => messenger.storage.local.set({ mailto_link_hover_color: e.target.value });
mailtoLinkRemoveUnderline.onchange = (e) => messenger.storage.local.set({ mailto_link_remove_underline: e.target.checked });
mailtoLinkDisableClick.onchange = (e) => messenger.storage.local.set({ mailto_link_disable_click: e.target.checked });
mailtoLinkNoException.onchange = (e) => messenger.storage.local.set({ mailto_link_no_exception: e.target.checked });





let otherLinkChangeAppearance = document.getElementById('other-link-change-appearance'),
    otherLinkNormalColor = document.getElementById('other-link-color'),
    otherLinkHoverColor = document.getElementById('other-link-hover-color'),
    otherLinkRemoveUnderline = document.getElementById('other-link-remove-underline');

otherLinkChangeAppearance.checked = options.other_link_change_appearance;
otherLinkNormalColor.value = options.other_link_color;
otherLinkHoverColor.value = options.other_link_hover_color;
otherLinkRemoveUnderline.checked = options.other_link_remove_underline;

if (otherLinkChangeAppearance.checked == false)
{
    document.querySelectorAll('.appearance-row')[1].classList.add('disabled');
    otherLinkNormalColor.disabled = otherLinkHoverColor.disabled = otherLinkRemoveUnderline.disabled = true;
}

otherLinkChangeAppearance.onchange = (e) =>
{
    let row = e.target.closest('div.box').querySelector('div.appearance-row');
    otherLinkNormalColor.disabled = otherLinkHoverColor.disabled = otherLinkRemoveUnderline.disabled = !e.target.checked;

    if (e.target.checked)
    {
        row.classList.remove('disabled');
    }
    else
    {
        row.classList.add('disabled');
    }

    messenger.storage.local.set({ other_link_change_appearance: e.target.checked });
}

otherLinkNormalColor.oninput = (e) => messenger.storage.local.set({ other_link_color: e.target.value });
otherLinkHoverColor.oninput = (e) => messenger.storage.local.set({ other_link_hover_color: e.target.value });
otherLinkRemoveUnderline.onchange = (e) => messenger.storage.local.set({ other_link_remove_underline: e.target.checked });

})();